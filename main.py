# Run this app with `python app.py` and
# visit http://127.0.0.1:8050/ in your web browser.

import dash
from dash import Dash, html, dcc
import plotly.express as px
import pandas as pd

# Loading Data
flights = pd.read_csv("flights.csv.gz")

# Data processing
red_eye1 = flights[flights['sched_dep_time']<600]
morning1 = flights[(flights['sched_dep_time']>=600) & (flights['sched_dep_time']<1200)]
afternoon1 = flights[(flights['sched_dep_time']>=1200) & (flights['sched_dep_time']<1600)]
evening1 = flights[(flights['sched_dep_time']>=1600) & (flights['sched_dep_time']<2400)]

red_eye_count1 = pd.value_counts(red_eye1['origin']).sort_index().tolist()
morning_count1 = pd.value_counts(morning1['origin']).sort_index().tolist()
afternoon_count1 = pd.value_counts(afternoon1['origin']).sort_index().tolist()
evening_count1 = pd.value_counts(evening1['origin']).sort_index().tolist()


red_eye2 = flights[flights['sched_arr_time']<600]
morning2 = flights[(flights['sched_arr_time']>=600) & (flights['sched_arr_time']<1200)]
afternoon2 = flights[(flights['sched_arr_time']>=1200) & (flights['sched_arr_time']<1600)]
evening2 = flights[(flights['sched_arr_time']>=1600) & (flights['sched_arr_time']<2400)]

red_eye_count2 = pd.value_counts(red_eye2['origin']).sort_index().tolist()
morning_count2 = pd.value_counts(morning2['origin']).sort_index().tolist()
afternoon_count2 = pd.value_counts(afternoon2['origin']).sort_index().tolist()
evening_count2 = pd.value_counts(evening2['origin']).sort_index().tolist()

# figure1
data1 = pd.DataFrame({'airport':['EWR','JFK','LGA']*4, 'time':['red_eye']*3 + ['morning']*3 + ['afternoon']*3 + ['evening']*3,
                     'count':red_eye_count1 + morning_count1 + afternoon_count1 + evening_count1})

fig1 = px.bar(data1, x="airport", y="count", color="time", title="Schedule-Departure_Count", height=800)

# figure2
data2 = pd.DataFrame({'airport':['EWR','JFK','LGA']*4, 'time':['red_eye']*3 + ['morning']*3 + ['afternoon']*3 + ['evening']*3,
                     'count':red_eye_count2 + morning_count2 + afternoon_count2 + evening_count2})

fig2 = px.bar(data2, x="airport", y="count", color="time", title="Schedule-Arrival_Count", height=800)


# Dash
app = Dash(__name__)
server = app.server

# assume you have a "long-form" data frame
# see https://plotly.com/python/px-arguments/ for more options
app.layout = html.Div(children=[
    # All elements from the top of the page
    html.Div([
        html.H1(children='Hello Dash'),

        html.Div(children='''
            Dash: A web application framework for Python.
        '''),

        dcc.Graph(
            id='graph1',
            figure=fig1
        ),
    ]),

    html.Div([
        html.H1(),

        html.Div(),

        dcc.Graph(
            id='graph2',
            figure=fig2
        ),
    ]),
])

if __name__ == '__main__':
    app.run_server(debug=True, port=8080)
